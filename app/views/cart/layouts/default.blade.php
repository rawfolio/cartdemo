<!DOCTYPE html>
<html>
<head>
	<title>Store Demo</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="{{ URL::to('/assets/css/bootstrap.min.css') }}" rel="stylesheet" media="screen">
	<link href="{{ URL::to('/assets/css/font-awesome.min.css') }}" rel="stylesheet" media="screen">
	<link href="{{ URL::to('/assets/css/demo.css') }}" rel="stylesheet" media="screen">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->

  </head>
  <body>
        <nav class="navbar xnavbar-fixed-top navbar-inverse" role="navigation">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="{{ URL::to('/') }}">Store</a>
        </div>

        <div class="collapse navbar-collapse navbar-ex1-collapse">
          <ul class="nav navbar-nav">
            <li{{ Request::is('/') ? ' class="active"' : null }}><a href="{{ URL::to('/') }}">Products</a></li>
            <li{{ Request::is('cart') ? ' class="active"' : null }}><a href="{{ URL::to('cart') }}">Cart <span class="badge cartCount">{{ app('cart')->items()->count() }}</span></a></li>
            <li{{ Request::is('wishlist') ? ' class="active"' : null }}><a href="{{ URL::to('wishlist') }}">Wishlist <span class="badge wishlistCount">{{ app('wishlist')->items()->count() }}</span></a></li>
          </ul>

          <ul class="nav navbar-nav navbar-right">
          
            @if (Sentinel::check())
            <li><a href="{{ URL::to('logout') }}">Logout</a></li>
            @else
            <li{{ Request::is('login') ? ' class="active"' : null }}><a href="{{ URL::to('login') }}">Login</a></li>
            @endif
          </ul>
        </div>
      </nav>
  	<div class="container">


  		@yield('page')
  	</div>

  	<script src="{{ URL::to('/assets/js/jquery.min.js') }}"></script>
  	<script src="{{ URL::to('/assets/js/bootstrap.min.js') }}"></script>

  	<script type="text/javascript">
  		$('.tip').tooltip();
  	</script>

  	@yield('scripts')

  	
  </body>
  </html>
